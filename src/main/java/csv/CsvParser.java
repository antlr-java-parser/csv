package csv;

import java.io.IOException;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;

import csv.antlr.CSVLexer;
import csv.antlr.CSVParser;

public class CsvParser extends CSVParser {
	
	public static CsvParser fromFilename(String filename)
	throws IOException {
		CharStream input = CharStreams.fromFileName(filename);
		return fromCharStream(input);
	}
	
	public static CsvParser fromString(String text) {
		CharStream input = CharStreams.fromString(text);
		return fromCharStream(input);
	}
	
	static CsvParser fromCharStream(CharStream input) {
		// create a lexer that feeds off of input CharStream
		CSVLexer lexer = new CSVLexer(input);
		// create a buffer of tokens pulled from the lexer
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		// create a parser that feeds off the tokens buffer
		CsvParser result = new CsvParser(tokens);
//		setupErrorListenersFor(List.of(lexer, result), attachDiagnostic);
		return result;
	}
	
	protected CsvParser(TokenStream input) { super(input); }
	
}
